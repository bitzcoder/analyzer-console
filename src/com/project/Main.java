package com.project;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner input = new Scanner(System.in);

        System.out.println("please enter original text");
        String originalText = input.nextLine();

        System.out.println("please enter clean text");
        String cleanText = input.nextLine();

        String missingChars = getMissingChar(originalText, cleanText);
        System.out.printf("%nMissing Text-----------------------------------------%n %s", missingChars);

    }

    /**
     * method determines the corresponding offset in the cleaned text.
     * @param originalText
     * @param cleanedText
     * @return
     */
    public static String getMissingChar(String originalText, String cleanedText){

        String missingString = "";

        List<Character> alreadyFound = new ArrayList<>();
        int characterCount[] = new int[255];

        for(int i=0; i < originalText.length(); i++ ){
            char currentChar = originalText.charAt(i);
            characterCount[(int)currentChar] += 1;
            if(cleanedText.indexOf(currentChar) == -1){
                missingString += currentChar;
            }else if((alreadyFound.contains(originalText.charAt(i)) && (characterCount[(int)currentChar] > countCharacterFreq(cleanedText, currentChar)))) {
                missingString += currentChar;
            }else{
                alreadyFound.add(currentChar);
            }
        }

        return missingString;
    }

    private static int countCharacterFreq(String cleanedText, Character selectedChar){
        int counter =0;
        for(int i=0; i < cleanedText.length(); i++ ){
            if(cleanedText.charAt(i) == selectedChar){
                counter++;
            }
        }

        return counter;
    }
}
